//	Initialize the copyWidget object
prism.run(["$filter", function($filter) {	

	//////////////////////////
	//	Plugin properties	//
	//////////////////////////

	//	Define allowed chart types
	var supportedCharts = ["chart/column","chart/line","chart/bar","chart/area"],
		supportedPivots = [],
		supportedTables = [],
		supportedIndicators = [];
	
	//	Value Label options
    var labelName = "Previous Value";
	var labelProperty = 'previousValue';


	//////////////////////////////////
	//	Business Logic Functions	//
	//////////////////////////////////

	var options = [
		{
			'label': 'add',
			'calculation': function(point, index, series){

				//	Save a reference to the original value
				point.yOriginal = point.y;

				//	Get the last point's value
				var lastValue = $$get(series, (index-1) + '.yOriginal', 0);

				//	Add the current value to the previous value
				point.y = lastValue + point.y;
				
				return null;
			}
		},{
			'label': 'subtract',
			'calculation': function(point, index, series){

				//	Save a reference to the original value
				point.yOriginal = point.y;

				//	Get the last point's value
				var lastValue = $$get(series, (index-1) + '.yOriginal', 0);

				//	Add the current value to the previous value
				point.y = point.y - lastValue;
				
				return null;
			}
		},{
			'label': 'Avg',
			'calculation': function(point, index, series){

				//	Save a reference to the original value
				point.yOriginal = point.y;

				//	Get the last point's value
				var lastValue = $$get(series, (index-1) + '.yOriginal', 0);

				//	Add the current value to the previous value
				point.y = (lastValue === 0) ? point.y : (lastValue + point.y) / 2;
				
				return null;
			}
		},{
			'label': 'Percent Change',
			'calculation': function(point, index, series){

				//	Save a reference to the original value
				point.yOriginal = point.y;

				//	Get the last point's value
				var lastValue = $$get(series, (index-1) + '.yOriginal', 0);

				//	Add the current value to the previous value (handle div/0 )
				point.y = (lastValue === 0) ? 0 : (lastValue + point.y) / lastValue;
				
				return null;
			}
		}

	]
    
	//////////////////////////
	//	Utility Functions	//
	//////////////////////////

	//	Function to determine if this chart type is supported
	function chartIsSupported(type){
		if (supportedCharts.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	function pivotIsSupported(type){
		if (supportedPivots.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	function tableIsSupported(type){
		if (supportedTables.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	function indicatorIsSupported(type){
		if (supportedIndicators.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//	function to find the value items from a widget
	function getItems(widget,section) {
		var valueItems = $.grep(widget.metadata.panels, function(w){
			return w.title===section;
		})[0];
		return valueItems;
	}

	//	Function to get series by name
	function getSeries(name, widget){

		return widget.queryResult.series.filter(function(i){ 
			return i.name == name 
		})
	}

	///////////////////////////
	//	Highcharts Functions //
	///////////////////////////

	function chartRenderOverride(widget, args){

		//	Loop through each metadata item, looking for time setting property
		widget.rawQueryResult.metadata.forEach(function(item){

			//	Do we need to use time formatting for this item?
			var calculationName = item[labelProperty];
			if (calculationName){

				//	Get a refernce to the calculation function
				var selectedOptions = options.filter(function(option){
					return option.label == calculationName;
				})

				//	Make sure we found the calculation
				var calculation = $$get(selectedOptions,'0.calculation');
				if (typeof calculation === "function"){

					//	Find any matching series
					var series = getSeries(item.jaql.title, widget)
					series.forEach(function(s){

						//	Loop through each data point, and run the function
						s.data.forEach(calculation)
					})
				}
			}
		})
	}

	function tableRenderOverride(widget,args){

		var changeValues = function(widget, element){

			//	Loop through each metadata item, looking for time setting property
			widget.rawQueryResult.metadata.forEach(function(item, index){

				//	Do we need to use time formatting for this item?
				var useTimeFormatting = item[labelProperty];
				if (useTimeFormatting){

					//	Loop through each row, finding the right column
					$('tbody > tr[role="row"] > td:nth-child(' + (index+1) +  ')').each(function(cell){

						// Parse the value
						var value = parseFloat($(this).text().replace(',','').replace('$',''));

						//	Write to the cell
						$(this).text( dateFormatter(value) );
					})
				}
			})
		}

		//  Define the fancy table handler
	    var tableHandler = function(e,settings,json){
	        changeValues(widget,widgetEl);
	    }

	    //	Get the div container, to scope jquery selectors
		var widgetEl = prism.$ngscope.appstate == "widget" ? $('.widget-body') : $('widget[widgetid="' + widget.oid + '"]');

	    //  Get a reference to the datatable object(s)
	    var myTables = $('table#grid',widgetEl);

	    //  Remove old handlers for tables that are going to be removed from dom
	    myTables.off('draw.dt',tableHandler);

	    //  Find the newest one
	    var newTable = $(myTables[myTables.length-1]).dataTable();  

	    //  Add event handler to handle redraws, paging, etc
	    newTable.on('draw.dt',tableHandler);

	    //	Run the first formatter
	    changeValues(widget,widgetEl);
	}

	function pivotRenderOverride(widget, args){

		//	Get the div container, to scope jquery selectors
		var widgetEl = prism.$ngscope.appstate == "widget" ? $('.widget-body') : $('widget[widgetid="' + widget.oid + '"]');

		//	Loop through each metadata item, looking for time setting property
		widget.metadata.panel('values').items.forEach(function(item, index){

			//	Do we need to use time formatting for this item?
			var useTimeFormatting = item[labelProperty];
			if (useTimeFormatting){

				//	Apply the text formatting for each cell in the table
				$('td.p-value[fidx=' + item.field.index + ']',widgetEl).each(function(i){
					$(this).text( dateFormatter( parseFloat( $(this).attr('val') ) ) );
				})	
			}
		})
	}

    //////////////////////////////////
    //  Initialization Functions    //
    //////////////////////////////////

	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		args.widget.on("destroyed", onWidgetDestroyed);
		//	Add hook on render
		if (chartIsSupported(args.widget.type)) {
			//	Highcharts event handlers
			args.widget.off("render", chartRenderOverride);
			args.widget.on("render", chartRenderOverride);
		} else if (pivotIsSupported(args.widget.type)) {
			//	pivot event handlers
			args.widget.off("domready", pivotRenderOverride);
			args.widget.on("domready", pivotRenderOverride);
		} else if (tableIsSupported(args.widget.type)) {
			//	table event handlers
			args.widget.off("domready", tableRenderOverride);
			args.widget.on("domready", tableRenderOverride);
		} 
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}


    //////////////////////////////////
    //  Menu  Functions             //
    //////////////////////////////////

    //  Function to make sure this is a settings menu for a measure
    function isValueMenu(settings) {
        
        //  Try to evaluate
        try {
            // Widget must be an allowed chart
            var supported = chartIsSupported(prism.$ngscope.widget.type) || pivotIsSupported(prism.$ngscope.widget.type) || tableIsSupported(prism.$ngscope.widget.type) || indicatorIsSupported(prism.$ngscope.widget.type)  ;       
            if(!supported){
                return false;
            }
            //  Check the menu's name
            if (settings.name !== "widget-metadataitem"){
                return false;
            }

            //  Check the jaql (except for table widget)
            if (prism.$ngscope.widget.type !== "tablewidget") {
	            if (!settings.jaql.agg && !settings.jaql.formula) {
	                return false;
	            }
	        }
        } catch(e) {
            return false;
        }
        return true;
    }

	//	Create Options for the menu
	prism.on("beforemenu", function (e, args) {
        
        //  Should the new menu item be added?
        var shouldAdd = isValueMenu(args.settings);
        if (shouldAdd) {
                
            //  Get the widget and the panel
            var panelItem = args.settings.item;          
            
            //  Get the list of existing items            
            var items = args.settings.items;

            //  Create a separator
            var separator = {
                type: "separator"
            };

            //  Function the runs when an item is picked
            var itemPicked = function(){
                
                //  set the property within the haql
                this.panelItem[labelProperty] = this.caption;
            
                //  Redraw the widget                
                prism.activeWidget.refresh();
            }

            //  Create menu for picking the regression measure
            var mainMenuItem = {
                caption: labelName,
                items: []
            };

            //	Loop through each option
            options.forEach(function(option){

            	//	Create a sub menu item
            	var subMenuItem = {
            		caption: option.label,
            		size: 'xl',
					type: 'check',
            		checked: option.label === panelItem.previousValue,
            		execute: itemPicked,
            		panelItem: panelItem
            	}

            	//	Save the sub menu item to the list
            	mainMenuItem.items.push(subMenuItem)
            })

            //   Add options to the menu         
            args.settings.items.push(separator);
            args.settings.items.push(mainMenuItem);
        }        
	});
}])